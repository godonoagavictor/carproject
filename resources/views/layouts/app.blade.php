<!DOCTYPE html>
<html>

@includeIf('layouts.partials.head')

<body>
    @includeIf('layouts.header')
    @yield('content')
    @includeIf('layouts.footer')

    @includeIf('layouts.partials.scripts')

</body>

</html>
